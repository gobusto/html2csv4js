/**
@brief Download the contents of a HTML table as a CSV file.

@param elem The DOM element representing the HTML table element to download.
@param name The filename to use for the CSV file (optional).
*/

function csvDownload(elem, name) {
  "use strict";
  var csv = [];
  var rows = elem.querySelectorAll("tr");
  for (var r = 0; r < rows.length; ++r) {
    var line = [];
    var cols = rows[r].querySelectorAll("th,td");
    for (var c = 0; c < cols.length; ++c) {
      line.push(cols[c].textContent.replace(/"/g, '""'));
    }
    csv.push('"' + line.join('","') + '"');
  }
  // Use a temporary `a` element (not `window.open()`) so we can set the name.
  var a = document.createElement("a");
  a.download = name || "output.csv";
  a.href = "data:text/csv," + encodeURIComponent(csv.join("\n"));
  a.target = "_blank"
  a.dispatchEvent(new MouseEvent('click', {view: window, bubbles: true, cancelable: true}));
}
